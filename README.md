# `decrypt_rs`

**Disclaimer:** This project is for Educational purposes only,
and should not be used for malicious and/or illegal purposes.

`decrypt_rs` is a Rust library providing cryptographic attacks, utils and helpers
for CTF and crypto challenges.

The project is still in its early stage of developpement, and basically
consists in implementing attacks for specific challenges in
a somewhat generic and reusable way.

The attacks have not been extensivelly tested on lots of different scenarios.
Therefore, their interfaces and functionalities might not be suitable for
all use cases.

Warning: The user of this library might make sure to understand what the functions
are doing before using them. Some detection functions and attacks might
send repetitive requests to the oracle.

`decrypt_rs` relies around the [`Oracle`](oracle::Oracle) trait which acts
as the interface with the crypto system.

