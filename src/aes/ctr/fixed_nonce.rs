// Copyright (C) See full copyright notice in the COPYRIGHT file at
// the root of the crate.
//
// This file is part of decrypt_rs.
//
//   decrypt_rs is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   decrypt_rs is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with decrypt_rs.  If not, see <https://www.gnu.org/licenses/>
//

use crate::{Result, Error};
use crate::substitution::Decoder;
use crate::substitution::alphabets::Alphabet;
use crate::substitution::KeyCandidates;

/// Implements an attack on CTR encrypted ciphertexts
/// when the nonce has been reused.
///
/// The structure needs to be given some ciphertexts of the **same length**
/// that has been encrypted with a block cipher using CTR mode of operation
/// with the same nonce.
///
/// The attack works by decoding the concatenated inputs as a
/// xor-substitution cipher with a key length equal to the length of the inputs.
/// The attack retrieve the most probable keystream that has been xored
/// with the inputs.
///
pub struct FixedNonceBreaker {
    freq_char: Vec<char>,
    input_text: Vec<u8>,
    key_size: Option<usize>,
}

impl FixedNonceBreaker {
    /// Structure constructor using predifined character frequency table
    pub fn new(alphabet: Alphabet, case_sensitive: bool) -> Self {
        Self::new_with_alpha(alphabet.get(case_sensitive).to_vec())
    }

    /// The `FixedNonceBreaker` is built in a similar way than
    /// the [`substitution::Decoder`](Decoder). This constructor
    /// allows to pass a custom character frequency table.
    pub fn new_with_alpha(freq_char: Vec<char>) -> Self {
        FixedNonceBreaker {
            freq_char,
            input_text: Vec::new(),
            key_size: None,
        }
    }

    /// Add 1 input ciphertext.
    ///
    /// All given ciphertexts must be the same length.
    ///
    /// This function consumes the given input.
    pub fn add_input(&mut self, mut input: Vec<u8>) -> Result<()> {
        if let Some(key_size) = self.key_size {
            if input.len() != key_size {
                return Err(Error::InvalidArgument("input", "Length of input should match with length of previous given inputs"));
            }
        } else {
            self.key_size = Some(input.len());
        }

        self.input_text.append(&mut input);

        Ok(())
    }

    /// Add multiple ciphertext inputs at once.
    ///
    /// All given ciphertexts must be the same length.
    pub fn add_inputs(&mut self, inputs: Vec<Vec<u8>>) -> Result<()> {
        for input in inputs {
            self.add_input(input)?;
        }

        Ok(())
    }

    /// Performs the attack.
    ///
    /// The function will use all given ciphertext inputs concatenated as
    /// a xor-substitution ciphertext. Then is will try to break each byte of the
    /// key.
    ///
    /// The attack retrieve probables keystream that has been xored
    /// with the block cipher plaintext inputs.
    pub fn break_keystream(&self) -> KeyCandidates {
        if let Some(key_size) = self.key_size {
            let mut decoder = Decoder::new_with_alpha(self.freq_char.clone()).poly().xor();

            decoder.break_key_with_keysize(&self.input_text, key_size)
        } else {
            KeyCandidates::new(0)
        }
    }
}
