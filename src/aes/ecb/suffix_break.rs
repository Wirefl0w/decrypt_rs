// Copyright (C) See full copyright notice in the COPYRIGHT file at
// the root of the crate.
//
// This file is part of decrypt_rs.
//
//   decrypt_rs is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   decrypt_rs is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with decrypt_rs.  If not, see <https://www.gnu.org/licenses/>
//

use crate::{Result, Error};
use crate::utils::{padding_size, pkcs7, hex};
use crate::oracle::CipherOracle;


/// This structure implements an attack to break a secret suffix appended to the data
/// given to an encryption [`Oracle`](CipherOracle) prior to encryption.
///
/// The attack performs byte-at-a-time ECB decryption in order to retrieve the
/// secret suffix.
pub struct SuffixBreaker {
    oracle: Box<dyn CipherOracle>,

    /// The block size _(in **bytes**)_ of the inner cipher.
    ///
    /// _(This value can be detected using [`SuffixBreaker::detect_block_size`])._
    pub block_size: usize,
    /// The length of an eventual prefix prepended to the input prior to encryption.
    ///
    /// _(This value can be detected using [`SuffixBreaker::detect_prefix_len`])._
    pub prefix_len: Option<usize>,
}

impl SuffixBreaker {
    /// Constructor of the `SuffixBreaker` struct.
    ///
    /// It takes the AES ECB [`CipherOracle`] that encrypts the data.
    ///
    /// Other necessary parameters might be affected directly or via their
    /// _build pattern_ methods _(see structure description for more details)_.
    pub fn new(oracle: Box<dyn CipherOracle>) -> Self {
        SuffixBreaker {
            oracle,
            block_size: 0,
            prefix_len: None,
        }
    }

    /// Sets the [prefix len](#structfield.prefix_len).
    pub fn prefix_len(mut self, prefix_len: usize) -> Self {
        self.prefix_len = Some(prefix_len);
        self
    }

    /// Sets the [prefix len](#structfield.prefix_len) to `0`.
    pub fn no_prefix(self) -> Self {
        self.prefix_len(0)
    }

    /// Sets the [block size](#structfield.block_size).
    pub fn block_size(mut self, block_size: usize) -> Self {
        self.block_size = block_size;

        self
    }


    /// Performs the whole attack.
    ///
    /// - It first tries to find the block size (skipped if `block_size > 0`)
    /// - Then tries to measure the length of the prefix if its `None`.
    /// - Verifes the [`CipherOracle`] produce some kind of expected output.
    /// - Finally, breaks the secret suffix byte by byte.
    ///
    /// It returns the broken suffix unpadded.
    pub fn break_suffix(&mut self) -> Result<Vec<u8>> {
        if self.block_size == 0 {
            self.detect_block_size()?;
            log::debug!("Detected block_size = {}", self.block_size);
        }

        if !self.check_ecb_mode()? {
            return Err(Error::IncorrectMode("Provided CipherOracle doesn't seem to encrypt using ECB mode"));
        }

        if self.prefix_len.is_none() {
            self.detect_prefix_len()?;
            log::debug!("prefix_len = {:?}", self.prefix_len);
        }

        let suffix_len = self.oracle.encrypt(b"").unwrap().len(); // (including padding)
        let mut suffix = Vec::with_capacity(suffix_len);
        log::debug!("Suffix len: {}", suffix_len);

        log::trace!("[ ] Breaking suffix:");

        let prefix_len = self.prefix_len
                .ok_or(Error::InvalidAttribute("prefix_len", "is unknown"))?;
        while let Some(b) = self.break_last_byte(&suffix, prefix_len)? {
            suffix.push(b);

            // Show avancement
            if log::log_enabled!(log::Level::Trace) {
                let len = suffix.len();

                if len > 10 {
                    log::trace!("[ ] ...{} ({})", hex::encode(&suffix[len-10..]), len);
                } else {
                    log::trace!("[ ]    {:>20}", hex::encode(&suffix));
                }
            }

        }

        if log::log_enabled!(log::Level::Trace) {
            log::trace!("[+] {}", hex::encode(&suffix));
        }

        pkcs7::unpad(suffix)
    }

    /// Try to detect the block size of the cipher used by the [`CipherOracle`].
    ///
    /// The found _block size_ if affected to the struct member and returned by the function.
    pub fn detect_block_size(&mut self) -> Result<usize> {
        self.block_size = super::detect_block_size(&mut self.oracle)?;

        Ok(self.block_size)
    }

    /// Verify if the [`CipherOracle`] use ECB mode of operation.
    ///
    /// The `block_size` should be determined prior to calling this function.
    pub fn check_ecb_mode(&mut self) -> Result<bool> {
        if self.block_size == 0 {
            return Err(Error::unknown_attribute("block_size"));
        }
        super::check_ecb(&mut self.oracle, self.block_size)
    }

    /// Try to detect the length of the prefix prepended by the [`CipherOracle`].
    ///
    /// The found _length_ if affected to the struct member and returned by the function.
    ///
    /// The `block_size` should be determined prior to calling this function.
    pub fn detect_prefix_len(&mut self) -> Result<Option<usize>> {
        if self.block_size == 0 {
            return Err(Error::unknown_attribute("block_size"));
        }

        let prefix_len = super::detect_input_prefix_len(&mut self.oracle, self.block_size);
        if let Ok(prefix_len) = prefix_len {
            self.prefix_len = prefix_len;
        }

        prefix_len
    }

    /// Break the next secret byte.
    ///
    /// Take the already known secret part as an argument to know where to look next.
    ///
    /// Returns the next secret byte or `None` if none corresponds.
    fn break_last_byte(&mut self, known_suffix: &[u8], prefix_len: usize) -> Result<Option<u8>> {
        let prefix_pad_len = if prefix_len > 0 {
            padding_size(prefix_len, self.block_size)
        } else {
            0
        };

        let prefix_full = prefix_len + prefix_pad_len;

        let mut input_block = Vec::with_capacity(self.block_size);
        let input_size = padding_size(known_suffix.len(), self.block_size) - 1 + prefix_pad_len;
        input_block.append(&mut vec![0x41; input_size]);


        let tgt_block_start = ((known_suffix.len() + prefix_full) / self.block_size) * self.block_size;
        let tgt_block_end   = tgt_block_start + self.block_size;

        let output = self.oracle.encrypt(&input_block)?;
        let target_block = &output[tgt_block_start .. tgt_block_end];

        input_block.append(&mut known_suffix.to_owned());

        let mut target_byte = None;

        for b in 0u8 ..= 255 {
            input_block.push(b);

            let output = self.oracle.encrypt(&input_block)?;
            let test_block = &output[tgt_block_start .. tgt_block_end];

            if test_block == target_block {
                target_byte = Some(b);
                break;
            }

            input_block.pop().unwrap();
        }

        Ok(target_byte)
    }
}
