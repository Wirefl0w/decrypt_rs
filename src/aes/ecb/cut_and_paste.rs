// Copyright (C) See full copyright notice in the COPYRIGHT file at
// the root of the crate.
//
// This file is part of decrypt_rs.
//
//   decrypt_rs is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   decrypt_rs is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with decrypt_rs.  If not, see <https://www.gnu.org/licenses/>
//

use crate::{Result, Error};
use crate::utils::{padding_size, pkcs7, hex};
use crate::oracle::CipherOracle;

/// This structure implement a "cut-and-paste" attack on AES ECB ciphertexts.
///
/// I tried to make it somewhat generic but didn't spend to much time on it
/// as ECB should not be very common these days.
///
/// The [`CipherOracle`] is expected to take some input, insert it
/// inside some plaintext \
/// (for example in a query string: `email={input}&uid=10&role=user`)
/// and return its ciphertext encrypted with AES ECB.
///
/// _Example, with an oracle inserting the input in the query string:
/// `email={input}&uid=10&role=user` prior encryption:_
/// ```text
/// let cnp = CutnPaste(Box::new(oracle))
///                         .block_size(16)
///                         .base_input("foo@bar.com")
///                         .input_pattern("email={input}&uid=10&role=user", "{input}");
///
/// let (input, input_enc) = cnp.paste_end("admin", "user".len());
/// // -> `input`: "foo@bar.com{probably some padding with 'spaces'}"
/// // -> `input_enc` is the ciphertext of the query string
///        with `input` given as input.
/// ```
/// _Currently the padding is performed using space and is not customizable._
pub struct CutnPaste {
    oracle:          Box<dyn CipherOracle>,
    /// The block size _(in **bytes**)_ of the inner cipher.
    pub block_size: usize,
    /// A valid input that can be sent to the oracle.
    pub base_input:  Vec<u8>,
    /// The `input_idx` represents the index in the plaintext where the input data
    /// given to the oracle is inserted. \
    /// _(For example in the query string: `email={input}&uid=10&role=user`,
    /// it corresponds to `"email=".len() = 6`.)_
    ///
    /// The variable can be determined automatically by the struct using [`CutnPaste::detect_input_idx`]
    /// or manually set by the user if known
    /// (or if one doesn't want to stress the oracle to much).
    pub input_idx:   Option<usize>,
    /// Represents the length of the data that is appended after the
    /// input. \
    /// _(For example for the query string `email={input}&uid=10&role=user`,
    /// it corresponds to `"&uid=10&role=user".len() = 17`.)_
    pub after_input: usize,
    /// Character used to pad blocks in the injected input.
    /// (Space by default)
    pub fill_char: u8,

}

impl CutnPaste {
    /// Constructor of the `CutnPaste` struct.
    ///
    /// It takes the AES ECB [`CipherOracle`] that encrypts the data.
    ///
    /// Other necessary parameters might be affected directly or via their
    /// _builder pattern_ methods _(see structure description for more details)_.
    ///
    pub fn new(oracle: Box<dyn CipherOracle>) -> Self {
        CutnPaste {
            oracle,
            block_size: 0,
            base_input: Vec::new(),
            input_idx: None,
            after_input: 0,
            fill_char: b' ',
        }
    }

    /// Sets the [block size](#structfield.block_size).
    pub fn block_size(mut self, block_size: usize) -> Self {
        self.block_size = block_size;
        self
    }

    /// Sets the [base input](#structfield.base_input).
    pub fn base_input<T: AsRef<[u8]>>(mut self, base_input: T) -> Self {
        self.base_input = base_input.as_ref().to_vec();
        self
    }

    /// Sets the [inpud idx](#structfield.input_idx).
    pub fn input_idx(mut self, input_idx: usize) -> Self {
        self.input_idx = Some(input_idx);
        self
    }

    /// Sets the [after input](#structfield.after_input).
    pub fn after_input(mut self, after_input: usize) -> Self {
        self.after_input = after_input;
        self
    }

    /// Sets the [filling char](#structfield.fill_char).
    pub fn fill_char(mut self, ch: u8) -> Self {
        self.fill_char = ch;
        self
    }


    /// If you know exactly the data in which the input data
    /// is inserted, you can use this function to both set
    /// [input_idx](#structfield.input_idx) and
    /// [after_input](#structfield.after_input).
    ///
    /// `input_pat` is the string in which the input is inserted prior to encryption
    /// and `pat` is a pattern representing the input inside `input_pat`. `pat` should
    /// appear only once in `input_pat`
    ///
    /// _For example, `input_pat = 'email={input}&uid=10&role=user'; pat = '{input}'`._
    pub fn input_pattern(mut self, input_pat: &str, pat: &str) -> Self {
        let split: Vec<&str> = input_pat.split(pat).collect();
        if split.len() != 2 {
            panic!("`pat` should appears once in `input_pat`");
        }

        let prefix = split[0];
        let suffix = split[1];

        // Rust documentation states that &str::len() method returns the length in bytes.
        self.input_idx = Some(prefix.len());
        self.after_input = suffix.len();

        self
    }

    /// Try to detect the length of the prefix prepended by the [`CipherOracle`].
    ///
    /// The found _length_ if affected to `self.input_idx` and returned by the function.
    pub fn detect_input_idx(&mut self) -> Result<Option<usize>> {
        if self.block_size == 0 {
            return Err(Error::unknown_attribute("block_size"));
        }

        let input_idx = super::detect_input_prefix_len(&mut self.oracle, self.block_size);
        if let Ok(input_idx) = input_idx {
            self.input_idx = input_idx;
        }

        input_idx
    }

    /// Perform a cut and paste attack and paste the given `data` at the end
    /// of the encrypted input.
    ///
    /// Additionnaly it can overwrite some bytes at the end by setting
    /// the `overwrite` argument to a value `>0`.
    ///
    /// The structure will craft a particular input to be sent to the oracle
    /// and it will rearrange the provided ciphertext, so when decrypted
    /// the provided `data` appears at the end of the plaintext, overwriting
    /// `overwrite` bytes at the end.
    ///
    /// The function returns the `input_data` and its corresponding crafted `ciphertext`.
    pub fn paste_end<T: AsRef<[u8]>>(&mut self, data: T, overwrite: usize) -> Result<(Vec<u8>, Vec<u8>)> {
        let data = data.as_ref();
        let input_idx = self.input_idx
                .ok_or(Error::InvalidAttribute("input_idx", "is unknown"))?;

        // After base_data, fill until end of block
        // (modulo block_size to avoid adding unnecessary padding)
        let base_pad_size = padding_size(input_idx + self.base_input.len(),
                            self.block_size) % self.block_size;
        let mut base_pad = vec![self.fill_char; base_pad_size];


        // After the data we want to cut, fill until end of block
        let data_pad_size = padding_size(data.len(), self.block_size) % self.block_size;
        let mut data_pad = vec![self.fill_char; data_pad_size];

        // Add padding so the data sent to the cipher (excluding the bytes to override)
        // ends at the end of a block.
        // This way we can append the data just after
        let final_pad_size = padding_size(self.after_input - overwrite, self.block_size) % self.block_size;
        let mut final_pad = vec![self.fill_char; final_pad_size];

        // A padding block will be added so we will be able to also cut and paste it
        // at the end of the crafted cipher to have valid padding.
        let mut padding_block = pkcs7::pad(b"", self.block_size);

        // Compute indices where to cut
        let cut_start     = input_idx + self.base_input.len() + base_pad.len();
        let cut_end       = cut_start + data.len() + data_pad.len();
        let pad_cut_start = cut_end;
        let pad_cut_end   = pad_cut_start + self.block_size;

        // Craft the input
        let input_size = self.base_input.len() + base_pad.len()
                            + data.len() + data_pad.len()
                            + self.block_size
                            + final_pad_size;
        let mut input = Vec::with_capacity(input_size);

        input.append(&mut self.base_input.clone()); // Base input
        input.append(&mut base_pad);                //  and padding

        input.append(&mut data.to_vec());           // Data to cut
        input.append(&mut data_pad);                //  and padding

        input.append(&mut padding_block);           // Add the AES padding block

        input.append(&mut final_pad.clone());       // More padding for the end of the plaintext
                                                    // to ends at the end of a full AES block.

        if log::log_enabled!(log::Level::Debug) {
            log::debug!("Crafted input: {}", hex::encode(&input));
            log::trace!("cut_start: {}", cut_start);
            log::trace!("cut_end:   {}", cut_end);
        }

        // Craft the cut input corresponding to the cut ciphertext
        let mut cut_input = Vec::with_capacity(cut_start + final_pad.len());
        cut_input.append(&mut input[..cut_start - input_idx].to_owned());
        cut_input.append(&mut final_pad);


        // Send crafted input to oracle
        let output = self.oracle.encrypt(input.as_ref())?;
        let output_end = output.len() - (overwrite + padding_size(overwrite, self.block_size));

        if log::log_enabled!(log::Level::Debug) {
            log::debug!("Received output: {}", hex::encode(&output));
        }

        // Craft the cut ciphertext
        let mut cut_output = Vec::with_capacity(output.len());
        cut_output.append(&mut output[..cut_start].to_owned());   // Up to the data to cut
        cut_output.append(&mut output[pad_cut_end..output_end].to_owned()); // From the data to cut up to the end
        cut_output.append(&mut output[cut_start..cut_end].to_owned()); // The cut data
        cut_output.append(&mut output[pad_cut_start..pad_cut_end].to_owned()); // The padding block

        Ok((cut_input, cut_output))
    }
}
