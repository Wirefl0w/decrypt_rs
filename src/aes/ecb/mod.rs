// Copyright (C) See full copyright notice in the COPYRIGHT file at
// the root of the crate.
//
// This file is part of decrypt_rs.
//
//   decrypt_rs is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   decrypt_rs is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with decrypt_rs.  If not, see <https://www.gnu.org/licenses/>
//

//! `ECB` mode of operation.
//!
//! # AES ECB
//!
//! This module provides various attacks on ECB mode operation for block ciphers.
//!

use crate::oracle::CipherOracle;
use crate::{Result, Error};

mod suffix_break;
mod cut_and_paste;

pub use suffix_break::SuffixBreaker;
pub use cut_and_paste::CutnPaste;


/// Count the number of equal `block_size` long blocks in `input`.
///
/// The occurences are counted only on blocks that are `block_size` aligned.
pub fn count_block_repetitions<T: AsRef<[u8]>>(input: T, block_size: usize) -> usize {
    let input = input.as_ref();
    let mut chunks: Vec<_> = input.chunks(block_size).collect();

    let mut repetitions = 0;

    while let Some(chunk) = chunks.pop() {
        if chunks.contains(&chunk) {
            // Count number of occurences of chunk in input
            repetitions += chunks.iter().filter(|x| *x == &chunk).count();
            // Remove current chunk from the rest of the input
            chunks.retain(|c| c != &chunk);
        }
    }

    repetitions
}

/// Find the index in `input` where two consecutive blocks are equal.
///
/// Start the search at index `start_block * block_size`.
pub fn first_block_repetition<T: AsRef<[u8]>>(input: T, block_size: usize, start_block: usize) -> Option<usize> {
    let input = input.as_ref();
    let start_idx = start_block * block_size;

    if start_idx >= input.len() - block_size {
        // `start_block` too far.
        return None;
    }

    let input = &input[start_idx..];

    let mut blocks = input.chunks(block_size);

    let mut block = blocks.next().expect("Empty input"); // Don't want to return an `Result` just
                                                         // for this
    let mut idx = start_idx;
    for block_next in blocks {
        if block == block_next {
            return Some(idx);
        }
        block = block_next;
        idx += block_size;
    }

    None
}


/// Detect the block size of the ECB cipher used by the [`CipherOracle`].
///
/// Returns `0` if the block size could not be determined.
///
/// Encrypts multiple times adding 1 input bytes each time until one more
/// output block is added. Returns the amount of bytes by which
/// the output has increased.
pub fn detect_block_size(oracle: &mut Box::<dyn CipherOracle>) -> Result<usize> {
    let max_input_len = 1024;

    let mut get_output_len = |input: &Vec<u8>| Ok::<usize, Error>(oracle.encrypt(input.as_ref())?.len());

    let mut input = Vec::with_capacity(max_input_len);

    input.push(0x41);
    let init_len = get_output_len(&input)?;
    input.push(0x41);
    let mut output_len = get_output_len(&input)?;

    while output_len == init_len && input.len() <= max_input_len {
        input.push(0x41);
        output_len = get_output_len(&input)?;
    }

    let block_size = output_len - init_len;

    if block_size == 0 || input.len() == max_input_len {
        // Failed to determine block size
        Ok(0)
    } else {
        Ok(block_size)
    }
}


/// Checks if the given [`CipherOracle`] use ECB encryption mode.
///
/// It encrypts 3 identical blocks and checks that there is at least 1 block repetition.
pub fn check_ecb(oracle: &mut Box<dyn CipherOracle>, block_size: usize) -> Result<bool> {
    let input = vec![0x41; block_size * 3];
    let output = oracle.encrypt(input.as_ref())?;

    Ok( count_block_repetitions(output, block_size) > 0 )
}

/// Given an encryption [`CipherOracle`] which prepends some prefix to the given input
/// prior to encryption, this function returns the size of said prefix,
/// or `None` if the prefix cannot be determined.
pub fn detect_input_prefix_len(oracle: &mut Box<dyn CipherOracle>, block_size: usize) -> Result<Option<usize>> {
    let max_input_len = 3 * block_size;
    let mut input = Vec::with_capacity(max_input_len);

    // Send at least 2 blocks to detect repetition
    input.append(&mut vec![0x41; 2 * block_size]);
    let mut add_bytes = 0;

    let mut rep_idx = first_block_repetition(oracle.encrypt(&input)?, block_size, 0);
    while rep_idx.is_none() && input.len() <= max_input_len {
        // Incrementally add some padding to identify how many bytes are missing
        // to the prefix to complete a full block
        add_bytes += 1;
        input.push(0x41);
        rep_idx = first_block_repetition(oracle.encrypt(input.as_ref())?, block_size, 0);
    }

    Ok(rep_idx.map(|rep_idx| rep_idx - add_bytes))
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_count_block() {
        let input = b"AAAABBBBAAAACCCCCCCCDDDDEEEEFFFFAAABAAAA";
        let exp_repeat = 3;

        assert_eq!(count_block_repetitions(input, 4), exp_repeat);
    }

    #[test]
    fn test_find_rep() {
        let input = b"AAAABBBBAAAACCCCCCCCDDDDEEEEFFFFAAABAAAA";
        let exp_idx = 12;      // ^ here

        assert_eq!(first_block_repetition(input, 4, 0), Some(exp_idx));
        assert_eq!(first_block_repetition(input, 4, 15), None);
    }
}
