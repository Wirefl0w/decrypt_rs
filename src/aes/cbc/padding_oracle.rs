// Copyright (C) See full copyright notice in the COPYRIGHT file at
// the root of the crate.
//
// This file is part of decrypt_rs.
//
//   decrypt_rs is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   decrypt_rs is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with decrypt_rs.  If not, see <https://www.gnu.org/licenses/>
//

use crate::{Result, Error};
use crate::oracle::DecipherOracle;
use crate::utils::hex;

/// This structure implements the CBC padding oracle attack.
///
/// Using a AES CBC deciphering oracle which tells if a given ciphertext
/// has valid padding or not, an attacker might retrieve the full
/// plaintext of an unknown ciphertext without knowing the key.
pub struct PaddingOracleBreaker {
    oracle: Box<dyn DecipherOracle>,

    /// The block size _(in **bytes**)_ of the inner cipher.
    pub block_size: usize,
    /// The IV used by the [`DecipherOracle`], if known.
    ///
    /// If the IV is not known, the attack will not retrieve the first block.
    ///
    /// Using the [`IvRetriever`](super::IvRetriever) might help.
    pub iv: Option<Vec<u8>>,
}

impl PaddingOracleBreaker {
    /// Constructor of the `PaddingOracleBreaker` struct.
    ///
    /// It takes the AES CBC [`DecipherOracle`] that encrypts the data.
    ///
    /// Other necessary parameters might be affected directly or via their
    /// _builder pattern_ methods _(see structure description for more details)_
    pub fn new(oracle: Box<dyn DecipherOracle>) -> Self {
        PaddingOracleBreaker {
            oracle,
            block_size: 0,
            iv: None,
        }
    }

    /// Sets the [block size](#structfield.block_size).
    pub fn block_size(mut self, block_size: usize) -> Self {
        self.block_size = block_size;
        self
    }

    /// Sets the [iv](#structfield.iv).
    pub fn iv<T: AsRef<[u8]>>(mut self, iv: T) -> Self {
        self.iv = Some(iv.as_ref().to_vec());
        self
    }

    /// Performs the actual attack.
    ///
    /// It takes the known ciphertext that has been encrypted with the same
    /// key and IV that the [`DecipherOracle`] uses for decrypting.
    ///
    /// If the IV isn't known, the attack will not break the first block.
    /// In this case, the returned output will only miss the first block of plaintext.
    pub fn decrypt_ciphertext<T: AsRef<[u8]>>(&mut self, ciphertext: T) -> Result<Vec<u8>> {
        let ciphertext = ciphertext.as_ref();
        self.check_padding_oracle(ciphertext)?;

        let mut plaintext = Vec::with_capacity(ciphertext.len());
        let ciphertext_data: Vec<u8>; // Ciphertext prepended by iv, if known

        if let Some(iv) = &self.iv {
            log::debug!("CBC padding oracle with IV");
            let mut ciphertext_iv = Vec::with_capacity(plaintext.len() + self.block_size);
            ciphertext_iv.append(&mut iv.clone());
            ciphertext_iv.append(&mut ciphertext.to_vec());
            ciphertext_data = ciphertext_iv;
        } else {
            log::debug!("CBC padding oracle without IV");
            ciphertext_data = ciphertext.to_vec();
        }

        let n_blocks = ciphertext_data.len() / self.block_size;
        log::debug!("Decrypting {} blocks", n_blocks);

        for block_idx in 1 .. n_blocks {
            log::trace!("[ ] block {} / {}:", block_idx, n_blocks-1);

            let mut block = self.decrypt_block(block_idx, &ciphertext_data)?;

            if log::log_enabled!(log::Level::Trace) {
                log::trace!("[+] {}", hex::encode(&block));
            }
            plaintext.append(&mut block);
        }

        Ok(plaintext)
    }


    /// Decrypts the `block_idx` block of the given `ciphertext`.
    ///
    /// `block_idx` should be `>=1` as we use the block before to decrypt. \
    /// `block_idx` should not be greated than `ciphertext.len() / block_size - 1`
    fn decrypt_block(&mut self, block_idx: usize, ciphertext: &[u8]) -> Result<Vec<u8>> {
        let block2_idx = block_idx * self.block_size;
        let block1_idx = block2_idx - self.block_size;

        let block1 = &ciphertext[block1_idx .. block2_idx];
        let block2 = &ciphertext[block2_idx .. block2_idx + self.block_size];

        let mut plain_block = vec![0u8; self.block_size];

        // Breaking the last byte first
        let ch_idx = self.block_size - 1;
        let last_byte = self.break_byte(ch_idx, block1, block2, &plain_block)
                            .ok_or(Error::Failure("Failed to retrieve last byte".to_string()))?;

        // Verifying last byte has produced valid padding alone with '\x01' and not
        // produced another valid padding with previous bytes
        let last_byte_correct = {
            let mut test_input = Vec::with_capacity(self.block_size * 2);
            test_input.append(&mut block1.to_vec());
            test_input.append(&mut block2.to_vec());

            // Set last byte to '\x01'
            test_input[ch_idx] = block1[ch_idx] ^ b'\x01' ^ last_byte;
            // Corrupt byte before
            test_input[ch_idx - 1] = block1[ch_idx - 1] ^ b'\xff';

            // If this doesn't produce valid padding, it means
            // previously found last byte wasn't '\x01', last byte isn't correct.
            self.oracle.decrypt(&test_input) != Err(Error::InvalidPadding)
        };

        if !last_byte_correct {
            // Try searching again
            let last_byte = self.break_byte_continue(ch_idx, last_byte+1, block1, block2, &plain_block)
                            .ok_or(Error::Failure("Failed to retrieve last byte".to_string()))?;
            plain_block[ch_idx] = last_byte;
        } else {
            plain_block[ch_idx] = last_byte;
        }

        // Iterate through each byte of the block from last to first.
        for ch_idx in (0 .. self.block_size-1).rev() {
            // Sets all the last bytes (after the one we try to retrieve)
            // to a valid padding configuration.
            let found_byte = self.break_byte(ch_idx, block1, block2, &plain_block);

            plain_block[ch_idx] = found_byte.ok_or(
                    Error::Failure(format!("Failed to find byte at idx {}", ch_idx))
                )?;
        }

        Ok(plain_block)
    }

    /// Decrypt the byte in `block2` at `idx` by manipulating `block1`,
    /// already knowing `plain_block` bytes in `block2`.
    ///
    /// It is assumed that `plain_block.len() >= idx`.
    fn break_byte(&mut self, idx: usize, block1: &[u8], block2: &[u8], plain_block: &[u8]) -> Option<u8> {
        self.break_byte_continue(idx, 0, block1, block2, plain_block)
    }

    /// `break_byte`, but starting the bruteforce at `continue_from`.
    fn break_byte_continue(&mut self, idx: usize, continue_from: u8, block1: &[u8], block2: &[u8], plain_block: &[u8]) -> Option<u8> {
        let mut test_input = Vec::with_capacity(self.block_size * 2);
        test_input.append(&mut block1.to_vec());
        test_input.append(&mut block2.to_vec());
        let pad_byte = (self.block_size - idx) as u8;

        for (i, plain_byte) in plain_block.iter().enumerate().skip(idx+1).rev() {
            test_input[i] = block1[i] ^ pad_byte ^ plain_byte;
        }

        let mut found_byte = None;

        for b in continue_from ..= 255u8 {
            test_input[idx] = block1[idx] ^ pad_byte ^ b;

            if log::log_enabled!(log::Level::Trace) {
                let max_displayed_len = 16;
                if self.block_size - idx <= max_displayed_len {
                    log::trace!("[ ] {:02x}{}", b, hex::encode(&plain_block[idx+1..]));
                } else {
                    log::trace!("[ ] {:02x}{}...", b, hex::encode(&plain_block[idx+1..idx+max_displayed_len]));
                }
            }

            if self.oracle.decrypt(test_input.as_ref()) != Err(Error::InvalidPadding) {
                found_byte = Some(b);
                break;
            }
        }

        found_byte
    }

    /// Checks if the provided [`DecipherOracle`] actually returns some padding error.
    ///
    /// The function takes a known ciphertext as an input and will
    /// proceed by tampering the padding bytes to check if a padding Error
    /// is returned.
    ///
    /// If it is not the case, the attack cannot be performed on the oracle.
    pub fn check_padding_oracle(&mut self, known_ciphertext: &[u8]) -> Result<()> {
        self.sanity_checks(known_ciphertext)?;

        let last_2_blocks_idx = known_ciphertext.len() - self.block_size * 2;
        let mut test_block = known_ciphertext[last_2_blocks_idx .. ].to_vec();

        for test_byte in 1 ..= 3 {
            test_block[self.block_size - 1] = test_byte;

            if self.oracle.decrypt(test_block.as_ref()) == Err(Error::InvalidPadding) {
                return Ok(());
            }
        }

        Err(Error::InvalidArgument("oracle", "Provided oracle doesn't seems to give invalid padding information"))
    }

    /// Performs all needed sanity checks around attribute settings
    /// and given known_ciphertext.
    fn sanity_checks(&mut self, known_ciphertext: &[u8]) -> Result<()> {
        if self.block_size == 0 {
            Err(Error::unknown_attribute("block_size"))
        } else if self.iv.as_ref().map_or(self.block_size, |iv| iv.len()) != self.block_size { // If is set, it should be block_size bytes long
            Err(Error::InvalidAttribute("iv", "Given iv is not 'block_size' bytes long"))
        } else if known_ciphertext.len() < self.block_size * 2 {
            Err(Error::InvalidArgument("known_ciphertext", "Given ciphertext should be at least 2 block long"))
        } else if known_ciphertext.len() % self.block_size != 0 {
            Err(Error::InvalidArgument("known_ciphertext", "Given ciphertext isn't aligned to block size"))
        } else {
            self.oracle.decrypt(known_ciphertext).or(
                    Err(Error::InvalidArgument("known_ciphertext", "DecipherOracle returns an error when decrypting given ciphertext"))
                )?;
            Ok(())
        }
    }
}
