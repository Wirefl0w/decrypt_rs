// Copyright (C) See full copyright notice in the COPYRIGHT file at
// the root of the crate.
//
// This file is part of decrypt_rs.
//
//   decrypt_rs is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   decrypt_rs is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with decrypt_rs.  If not, see <https://www.gnu.org/licenses/>
//

//! `CBC` mode of operation.
//!
//! # AES CBC
//!
//! This module provides various attacks on CBC mode operation for block ciphers.
//!

mod padding_oracle;
mod bitflipping;
mod iv_retrieval;

pub use bitflipping::bit_flipper;
pub use padding_oracle::PaddingOracleBreaker;
pub use iv_retrieval::IvRetriever;
