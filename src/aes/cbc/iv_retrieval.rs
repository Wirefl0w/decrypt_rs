// Copyright (C) See full copyright notice in the COPYRIGHT file at
// the root of the crate.
//
// This file is part of decrypt_rs.
//
//   decrypt_rs is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   decrypt_rs is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with decrypt_rs.  If not, see <https://www.gnu.org/licenses/>
//

use crate::{Result, Error};
use crate::oracle::DecipherOracle;
use crate::utils::{xor, hex};

/// This structure can be used to extract the IV
/// of a CBC [deciphering oracle](DecipherOracle).
pub struct IvRetriever {
    oracle: Box<dyn DecipherOracle>,

    /// The block_size _(in **bytes**)_ of the inner cipher.
    pub block_size: usize,
}

impl IvRetriever {
    /// Constructor of the `IvRetriever` struct.
    ///
    /// It takes the AES CBC deciphering [`DecipherOracle`].
    pub fn new(oracle: Box<dyn DecipherOracle>) -> Self {
        IvRetriever {
            oracle,
            block_size: 0,
        }
    }

    /// Sets the [block size](#structfield.block_size).
    pub fn block_size(mut self, block_size: usize) -> Self {
        self.block_size = block_size;
        self
    }

    /// Retrieves the IV from the given oracle.
    ///
    /// The function should be provided with a valid ciphertext
    /// produced by the oracle cipher that is at least 3 blocks long.
    ///
    /// The function produce an input that consists in `[block1 | 0 | block1 | valid | padding]`
    /// and will xor the first and third outcoming plaintext block.
    pub fn retrieve_iv<T: AsRef<[u8]>>(&mut self, ciphertext: T) -> Result<Vec<u8>> {
        if self.block_size == 0 {
            return Err(Error::unknown_attribute("block_size"));
        }

        let ciphertext = ciphertext.as_ref();

        if ciphertext.len() < 3 * self.block_size {
            return Err(Error::InvalidAttribute("ciphertext", "Should be at least 3 blocks long"));
        }

        // Get the first block
        let mut block1: Vec<u8> = ciphertext.iter()
                                            .take(self.block_size)
                                            .copied()
                                            .collect();
        // Keep valid padding using 2 last blocks
        let mut padding = ciphertext.iter()
                                    .skip(ciphertext.len() - 2 * self.block_size)
                                    .take(2 * self.block_size)
                                    .copied()
                                    .collect();


        let mut input = Vec::with_capacity(3 * self.block_size);
        input.append(&mut block1.clone());
        input.append(&mut vec![0u8;self.block_size]);
        input.append(&mut block1);
        input.append(&mut padding);

        let dec_data = self.oracle.decrypt(&input)?;

        if log::log_enabled!(log::Level::Trace) {
            log::trace!("Crafted input:   {}", hex::encode(&input));
            log::trace!("Received output: {}", hex::encode(&dec_data));
        }

        let block1 = &dec_data[..self.block_size];
        let block3 = &dec_data[self.block_size * 2 .. self.block_size * 3];

        Ok(xor(block1, block3))
    }

}
