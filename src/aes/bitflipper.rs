// Copyright (C) See full copyright notice in the COPYRIGHT file at
// the root of the crate.
//
// This file is part of decrypt_rs.
//
//   decrypt_rs is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   decrypt_rs is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with decrypt_rs.  If not, see <https://www.gnu.org/licenses/>
//

use crate::{Result, Error};
use crate::oracle::CipherOracle;
use crate::utils::{pad, hex};

/// This structure implements the bitflipping attack for
/// `CBC` and `CTR` mode of operations.
///
/// This struct is usually created using either
/// the [`cbc::BitFlipper`](super::cbc::BitFlipper) or
/// the [`ctr::BitFlipper`](super::ctr::BitFlipper) from
/// the appropriate module.
///
/// The attack allows to change some bytes in a plaintext by changing
/// bytes of the previous block in the corresponding ciphertext.
pub struct CbcCtrBitFlipper {
    oracle: Box<dyn CipherOracle>,
    mode_cbc: bool,

    /// The block size _(in **bytes**)_ of the inner cipher.
    pub block_size: usize,
    /// The length of an eventual prefix prepended to the input prior to encryption.
    pub prefix_len: Option<usize>,
    /// Optional data that will be inserted first in the input field sent to the oracle.
    pub base_input: Vec<u8>,

}

impl CbcCtrBitFlipper {
    /// Constructor of the `BitFlipper` struct.
    ///
    /// It takes the AES [`CipherOracle`] that encrypts the data as
    /// well as the mode of operation as a boolean:
    ///  - `true` for `CBC`
    ///  - `false` for `CTR`
    fn new(oracle: Box<dyn CipherOracle>, mode_cbc: bool) -> Self {
        CbcCtrBitFlipper {
            oracle,
            mode_cbc,
            block_size: 0,
            prefix_len: None,
            base_input: Vec::new(),
        }
    }

    /// Construct a CBC bitflipper.
    ///
    /// Other necessary parameters might be affected directly or via their
    /// _builder pattern_ methods _(see structure description for more details)_.
    ///
    pub fn new_cbc(oracle: Box<dyn CipherOracle>) -> Self {
        Self::new(oracle, true)
    }

    /// Construct a CTR bitflipper.
    ///
    /// Other necessary parameters might be affected directly or via their
    /// _builder pattern_ methods _(see structure description for more details)_.
    ///
    pub fn new_ctr(oracle: Box<dyn CipherOracle>) -> Self {
        Self::new(oracle, false)
    }


    /// Sets the [block size](#structfield.block_size).
    pub fn block_size(mut self, block_size: usize) -> Self {
        self.block_size = block_size;
        self
    }

    /// Sets the [prefix len](#structfield.prefix_len).
    pub fn prefix_len(mut self, prefix_len: usize) -> Self {
        self.prefix_len = Some(prefix_len);
        self
    }

    /// Sets the [base input](#structfield.base_input).
    pub fn base_input<T: AsRef<[u8]>>(mut self, base_input: T) -> Self {
        self.base_input = base_input.as_ref().to_vec();
        self
    }

    /// Performs the actual attack.
    ///
    /// It needs the data we want to write and a list of forbidden chars.
    /// At least one char of the data should be allowed.
    ///
    /// The function crafts a specific input with valid chars in place of forbidden ones.
    /// It then, feeds it to the oracle and update the produced ciphertext to flip the
    /// necessary bytes so the given `data` appears in the plaintext when decrypted.
    pub fn bit_flip<T: AsRef<[u8]>>(&mut self, data: T, bad_chars: &[u8]) -> Result<Vec<u8>> {
        let data = data.as_ref();

        if log::log_enabled!(log::Level::Debug) {
            let mode = if self.mode_cbc { "CBC" } else { "CTR" };
            log::debug!("{} Bitflipping", mode);
        }

        // Input checks
        let prefix_len = self.prefix_len
                .ok_or(Error::unknown_attribute("prefix_len"))?;
        if self.block_size == 0 {
            return Err(Error::unknown_attribute("block_size"));
        }
        if data.len() > self.block_size {
            return Err(Error::InvalidArgument("data", "Data size should not be greater than block size"));
        }

        // Find a valid character in given data
        let mut dummy_char = None;
        for ch in data {
            if !bad_chars.contains(ch) {
                dummy_char = Some(ch);
            }
        }

        let dummy_char = dummy_char
                .ok_or(Error::InvalidArgument("data", "Given data doesn't conatins any valid bytes."))?;

        log::trace!("Using dummy char: 0x{:02x}", dummy_char);

        // Creates valid data by replacing bad chars with valid ones
        let valid_data = data.iter()
                            .map(|c| if bad_chars.contains(c) { dummy_char } else { c })
                            .copied()
                            .collect::<Vec<u8>>();

        // Get the indices where bad chars are in data
        let indices = data.iter().enumerate()
                                .filter_map(|(i, c)| if bad_chars.contains(c) { Some(i) } else { None })
                                .collect::<Vec<usize>>();

        // Build the input data to feed the oracle

        let mut base_padded = Vec::new();
        if !self.base_input.is_empty() {
            base_padded = pad(&self.base_input, self.block_size, *dummy_char);
        }
        let mut valid_padded = pad(&valid_data, self.block_size, *dummy_char);

        let mut input_data = Vec::with_capacity(base_padded.len() + self.block_size + valid_padded.len());
        input_data.append(&mut base_padded);

        if self.mode_cbc {
            let mut dummy_block = vec![*dummy_char; self.block_size];
            input_data.append(&mut dummy_block);
        }

        input_data.append(&mut valid_padded);

        if log::log_enabled!(log::Level::Debug) {
            log::debug!("Crafted input:   {}", hex::encode(&input_data));
        }

        let mut output = self.oracle.encrypt(input_data.as_ref())?;

        if log::log_enabled!(log::Level::Debug) {
            log::debug!("Received output: {}", hex::encode(&output));
        }

        // Flip the bits !
        for idx in indices {
            let out_idx = prefix_len + idx;
            output[out_idx] = output[out_idx] ^ valid_data[idx] ^ data[idx];
        }

        Ok(output)
    }
}
