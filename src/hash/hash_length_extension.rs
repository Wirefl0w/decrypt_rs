// Copyright (C) See full copyright notice in the COPYRIGHT file at
// the root of the crate.
//
// This file is part of decrypt_rs.
//
//   decrypt_rs is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   decrypt_rs is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with decrypt_rs.  If not, see <https://www.gnu.org/licenses/>
//

use crate::Result;
use crate::oracle::HashInterface;
use crate::utils::padding_size;

/// Struct implementing Hash length extension attack.
///
/// The attack concerns poorly implemented hashmac algorithms that
/// simply hashes the concatenation of the key and the data to produce
/// a token (`hash(key || data) = token`, where `||` indicates concatenation).
///
/// The attack exploits the property of some hash functions to be able
/// to compute `hash(message1 || message2)` with the knowledge of only
/// `message2`, `hash(message1)` and the length of `message1`
/// (to know the padding appended to `message1`).
///
/// The struct needs an implementation of the hashing algorithm through
/// the [`HashInterface`] trait. Said trait gives access to the
/// internal state which is used to start processing the hash from a given state,
/// as well as a method that computes the padding for a given input length.
///
/// The [`HashInterface`] traits externalises the hash function implementation
/// from the library. Users of this struct might need to modify and existing
/// implementation to provides traits methods or to provide their own hash
/// function implementation.
///
/// An example can be found for a `sha1` hash in `example/sha1_hash_interface.rs`.
///
/// In the case of a service that computes `hash(key || data) = digest`, knowing
/// `data`, `digest` and the length of the `key`, the attack can add some additional
/// data (part of which is under attacker control) and provides a valid digest such as \
/// `hash(key || data || additional_data) == new_digest`. \
///
/// In that case, the attack provides `new_data = data || additional_data` and
/// `new_digest`.
///
/// The `additional_data` consists in `padding || controlled_data`,
/// where `padding` corresponds to the padding bytes internally generated
/// by the hash function when hashing `key || data`.
///
pub struct HashLengthExtension {
    hash: Box<dyn HashInterface>,
    data: Vec<u8>,
    digest: Vec<u8>,
    append_data: Vec<u8>,
}

impl HashLengthExtension {
    /// Initial constructor taking the [`HashInterface`].
    pub fn new(hash: Box<dyn HashInterface>) -> Self {
        HashLengthExtension {
            hash,
            data: Vec::new(),
            digest: Vec::new(),
            append_data: Vec::new(),
        }
    }

    /// Sets the initial data on which additional data will be appended.
    pub fn initial_data<T: AsRef<[u8]>>(mut self, data: T) -> Self {
        self.data = data.as_ref().to_vec();
        self
    }

    /// Sets the digest corresponding the the initial data.
    pub fn initial_digest<T: AsRef<[u8]>>(mut self, digest: T) -> Self {
        self.digest = digest.as_ref().to_vec();
        self
    }

    /// Sets the data that will be appended.
    pub fn append_data<T: AsRef<[u8]>>(mut self, data: T) -> Self {
        self.append_data = data.as_ref().to_vec();
        self
    }

    /// Performs the Hash Length Extension attack.
    ///
    /// Returns a tuple with respectively the _new data_ and
    /// the correspondig _new digest_ such as \
    /// `hash(key || new_data) == new_digest`.
    pub fn rehash(&mut self, key_len: usize) -> Result<(Vec<u8>, Vec<u8>)> {
        //
        // === Compute `new_data` ===
        //

        // Compute initial padding
        let initial_len = key_len + self.data.len(); // The length of the message initially hashed
        let mut initial_data_padding = self.hash.padding(initial_len)?;

        let mut crafted_data = Vec::with_capacity(
            self.data.len() +
            initial_data_padding.len() +
            self.append_data.len()
        );

        crafted_data.append(&mut self.data.clone());
        crafted_data.append(&mut initial_data_padding);
        crafted_data.append(&mut self.append_data.clone());

        //
        // === Compute `new_digest` ===
        //

        // Initialize hash state from `initial_digest`
        self.hash.set_state(&self.digest)?;

        // Compute the padding of the new message:
        // `key || initial_data || initial_padding || append_data`
        let mut padding = self.hash.padding(
            initial_len +
            padding_size(initial_len, self.hash.block_size()) +
            self.append_data.len()
        )?;

        let mut hash_input = Vec::with_capacity(self.append_data.len() + padding.len());
        hash_input.append(&mut self.append_data.clone());
        hash_input.append(&mut padding);

        let digest = self.hash.process_blocks(&hash_input)?;

        Ok((crafted_data, digest))
    }

    /// Static function performing the attack in one call.
    ///
    /// Performs the attack without the need for the user to instantiate the struct.
    pub fn hle(hash: Box<dyn HashInterface>,
               data: impl AsRef<[u8]>,
               digest: impl AsRef<[u8]>,
               append_data: impl AsRef<[u8]>,
               key_len: usize) -> Result<(Vec<u8>, Vec<u8>)> {
        let mut h = HashLengthExtension::new(hash)
                    .initial_data(data)
                    .initial_digest(digest)
                    .append_data(append_data);

        h.rehash(key_len)
    }
}
