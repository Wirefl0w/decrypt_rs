// Copyright (C) See full copyright notice in the COPYRIGHT file at
// the root of the crate.
//
// This file is part of decrypt_rs.
//
//   decrypt_rs is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   decrypt_rs is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with decrypt_rs.  If not, see <https://www.gnu.org/licenses/>
//

//! Generic utilities.

pub mod pkcs7;
pub mod hex;

use super::substitution::Cipher as SubsCipher;

/// Compute the hamming distance (number of differing bits) between two byte arrays.
pub fn hamming_distance(a: impl AsRef<[u8]>, b: impl AsRef<[u8]>) -> usize {
    let short: &[u8];
    let long:  &[u8];

    if a.as_ref().len() < b.as_ref().len() {
        short = a.as_ref();
        long  = b.as_ref();
    } else {
        short = b.as_ref();
        long  = a.as_ref();
    }

    // Count the differing bits between long and short
    let hamming: usize = short.iter().enumerate()
                                     .map(|(i, x)| (x ^ long[i]).count_ones() as usize)
                                     .sum();

    // Add the number of bits missing for short to be as long as long.
    hamming + (long.len() - short.len()) * 8
}

/// Xor both inputs together.
///
/// If one input is shorter than the other,
/// the shortest input is repeated to match the length of the longest.
pub fn xor(a: impl AsRef<[u8]>, b: impl AsRef<[u8]>) -> Vec<u8> {
    let short: &[u8];
    let long:  &[u8];

    if a.as_ref().len() < b.as_ref().len() {
        short = a.as_ref();
        long  = b.as_ref();
    } else {
        short = b.as_ref();
        long  = a.as_ref();
    }

    SubsCipher::new().xor().encode_with_key(long, short)
}

/// Returns the number of bytes to add to an input of `input_len` bytes
/// for its size to be a multiple of `block_size` bytes.
#[inline]
pub fn padding_size(input_len: usize, block_size: usize) -> usize {
    block_size - (input_len % block_size)
}

/// Pad the given `input` at the given `block_size` with the given `pad_byte`.
///
/// The function returns a new `Vec` containing the input and the padding.
pub fn pad<T: AsRef<[u8]>>(input: T, block_size: usize, pad_byte: u8) -> Vec<u8> {
    let input = input.as_ref();

    let pad_size = padding_size(input.len(), block_size);
    let mut padding = vec![pad_byte; pad_size];

    let mut padded = Vec::with_capacity(input.len() + pad_size);
    padded.append(&mut input.to_vec());
    padded.append(&mut padding);

    padded
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn fixed_xor() {
        let a = b"\x1c\x01\x11\x00\x1f\x01\x01\x00\x06\x1a\x02\x4b\x53\x53\x50\x09\x18\x1c";
        let b = b"\x68\x69\x74\x20\x74\x68\x65\x20\x62\x75\x6c\x6c\x27\x73\x20\x65\x79\x65";

        let expected = b"\x74\x68\x65\x20\x6b\x69\x64\x20\x64\x6f\x6e\x27\x74\x20\x70\x6c\x61\x79";

        assert_eq!(xor(a, b), expected);
    }

    #[test]
    fn repeat_key_xor() {
        let input = b"Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal";
        let expected = b"\x0b\x36\x37\x27\x2a\x2b\x2e\x63\x62\x2c\x2e\x69\x69\x2a\x23\x69\x3a\x2a\x3c\x63\x24\x20\x2d\x62\x3d\x63\x34\x3c\x2a\x26\x22\x63\x24\x27\x27\x65\x27\x2a\x28\x2b\x2f\x20\x43\x0a\x65\x2e\x2c\x65\x2a\x31\x24\x33\x3a\x65\x3e\x2b\x20\x27\x63\x0c\x69\x2b\x20\x28\x31\x65\x28\x63\x26\x30\x2e\x27\x28\x2f";

        let key = b"ICE";

        assert_eq!(xor(input, key), expected);
    }

    #[test]
    fn test_hamming() {
        let a = "this is a test";
        let b = "wokka wokka!!!";

        let expected = 37;

        assert_eq!(hamming_distance(a, b), expected);
    }
}
