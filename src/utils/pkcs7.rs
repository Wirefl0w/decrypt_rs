// Copyright (C) See full copyright notice in the COPYRIGHT file at
// the root of the crate.
//
// This file is part of decrypt_rs.
//
//   decrypt_rs is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   decrypt_rs is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with decrypt_rs.  If not, see <https://www.gnu.org/licenses/>
//

//! Provides utils for padding and unpadding following the `pkcs#7` padding scheme.
//!
//! See [`RFC5652 Section 6.3`](https://datatracker.ietf.org/doc/html/rfc5652#section-6.3)

use crate::{Result, Error};

/// Pad the given `input` at the given `size`.
///
/// The function returns a new `Vec` containing the input and the padding.
pub fn pad<T: AsRef<[u8]>>(input: T, size: usize) -> Vec<u8> {
    let input = input.as_ref();
    let missing_bytes = size - (input.len() % size);

    let mut padding = vec![missing_bytes as u8; missing_bytes];

    let mut output = Vec::with_capacity(input.len() + padding.len());
    output.append(&mut input.to_vec());
    output.append(&mut padding);

    output
}

/// Unpad the given `input` data.
///
/// The function returns a new `Vec` containing the unpadded input if
/// given input contains a valid padding scheme.
///
/// Returns [`Error::InvalidPadding`] in the case the input does not contains
/// a valid padding scheme and [`Error::InvalidArgument`] if `input` is empty.
pub fn unpad<T: AsRef<[u8]>>(input: T) -> Result<Vec<u8>> {
    let mut output = input.as_ref().to_vec();

    if let Some(pad_size) = output.last() {
        let unpadded_len = output.len() as isize - *pad_size as isize;
        if *pad_size > 0 && unpadded_len > 0 && output[unpadded_len as usize..] == vec![*pad_size; *pad_size as usize] {
            output.truncate(unpadded_len as usize);
            Ok(output)
        } else {
            Err(Error::InvalidPadding)
        }
    } else {
        Err(Error::InvalidArgument("input", "is empty"))
    }
}

