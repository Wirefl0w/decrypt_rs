// Copyright (C) See full copyright notice in the COPYRIGHT file at
// the root of the crate.
//
// This file is part of decrypt_rs.
//
//   decrypt_rs is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   decrypt_rs is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with decrypt_rs.  If not, see <https://www.gnu.org/licenses/>
//

//! Hexstring encoding / decoding.
//!
//! Functions in this module are not really optimized.
//! They are used internally for debug purposes and are
//! here to avoid adding dependencies.

/// Encodes an array of bytes into an hex string.
pub fn encode<T: AsRef<[u8]>>(input: T) -> String {
    input.as_ref().iter()
        .map(|x| format!("{:02x}", x))
        .collect::<Vec<String>>()
        .concat()
}

/// Decodes a hexstring into a `Vec<u8>`.
///
/// Returns `None` in case the given input is not a valid hexstring.
pub fn decode(input: &str) -> Option<Vec<u8>> {
    if input.len() % 2 == 0 {
        (0..input.len())
            .step_by(2)
            .map(|i| input.get(i..i+2)
                      .and_then(|x| u8::from_str_radix(x, 16).ok()))
            .collect()
    } else {
        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    static BYTES: &[u8] = b"Yellow Submarine";
    static HEXSTR: &str = "59656c6c6f77205375626d6172696e65";

    #[test]
    fn test_encode() {
        assert_eq!(encode(&BYTES), HEXSTR);
    }

    #[test]
    fn test_decode() {
        assert_eq!(decode(&HEXSTR), Some(BYTES.to_vec()));

        assert_eq!(decode(&HEXSTR[1..]), None);
        assert_eq!(decode("Non hex chars"), None);
    }
}
