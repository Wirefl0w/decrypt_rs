// Copyright (C) See full copyright notice in the COPYRIGHT file at
// the root of the crate.
//
// This file is part of decrypt_rs.
//
//   decrypt_rs is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   decrypt_rs is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with decrypt_rs.  If not, see <https://www.gnu.org/licenses/>
//

use crate::Result;

/// Trait for Hashing cryptosystems.
///
/// This traits is used to provides useful low-level methods for some
/// attacks (like [Hash Length Extension](crate::hash::HashLengthExtension))
/// which needs to access some internals of the algorithm.
///
/// For these attacks the Hashing algorithm might be reimplemented
/// (or an existing implementation might be modified) in order
/// to implement the trait methods.
///
/// _See in the `examples` folder for an implementation
/// of this trait for a `sha1` hash function._
///
/// The type should provide a `process_blocks` method
/// which updates the hash (without appending padding)
/// with given data which should be aligned
/// to the algorithm block size.
///
/// In addition to the previous method, the type also
/// needs to provide the padding function and methods
/// to give access to the internal hash state.
///
/// The traits provides methods needed for some attacks but not necessarily
/// handy methods one would expect from a high-level hashing interface.
pub trait HashInterface {
    /// The size (in bytes) of the digest procuded by the algorithm.
    fn digest_size(&self) -> usize;
    /// The size (in bytes) of the blocks processed by the algorithm.
    fn block_size(&self) -> usize;

    /// Returns the padding data that would be appended to
    /// an input of the given size before processing it.
    fn padding(&self, input_size: usize) -> Result<Vec<u8>>;

    /// Sets the internal state of the hash function from a output digest.
    fn set_state(&mut self, digest: &[u8]) -> Result<()>;

    /// Hash given data without adding padding.
    ///
    /// The data should be aligned with the algorithm block size.
    ///
    /// The `process_blocks` method can be seen as a low-level call to
    /// the hash algorithm.
    fn process_blocks(&mut self, input: &[u8]) -> Result<Vec<u8>>;
}

