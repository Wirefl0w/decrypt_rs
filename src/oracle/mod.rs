// Copyright (C) See full copyright notice in the COPYRIGHT file at
// the root of the crate.
//
// This file is part of decrypt_rs.
//
//   decrypt_rs is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   decrypt_rs is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with decrypt_rs.  If not, see <https://www.gnu.org/licenses/>
//

//! Cryptosystem interfaces.
//!
//! The `Oracle` traits are the main interface between a crypto system and
//! the `decrypt_rs` library.
//!
//! Most attacks in the library take an `Oracle` as an argument in order
//! to break its inner crypto. You should implement the `Oracle` trait
//! by encapsulating the crypto system interface between a single
//! `process` function that take an input and returns an output.
//!
//! When implementing a [`CipherOracle`] or [`DecipherOracle`], only the super trait
//! [`Oracle::process`] method needs to be implemented. The other sub traits
//! methods wrapps [`Oracle::process`] (See example below).
//!
//! [`HashInterface`] however, works differently and does not depend
//! on the [`Oracle`] trait.
//!
//! ## Example
//!
//! An example implementation for a `CipherOracle` which
//! interacts with a tcp service that encrypts data through AES.
//!
//! ```no_run
//! use std::io::{Read, Write};
//! use std::net::TcpStream;
//! use decrypt_rs::oracle::{Oracle, CipherOracle};
//! use decrypt_rs::{Result, Error};
//!
//! // Define an struct interacting with the target cryptosystem
//! struct AesServiceOracle {
//!     tcp: TcpStream,
//! }
//!
//! impl AesServiceOracle {
//!     pub fn new() -> Self {
//!         let tcp = TcpStream::connect("service_addr:service_port").unwrap();
//!
//!         AesServiceOracle {
//!             tcp,
//!         }
//!     }
//! }
//!
//! // Implements the `Oracle` trait.
//! impl Oracle for AesServiceOracle {
//!     fn process(&mut self, input: &[u8]) -> Result<Vec<u8>> {
//!         self.tcp.write(input)
//!                 .map_err(|err| Error::Failure(err.to_string()))?;
//!
//!         let mut output = [0u8;512];
//!         self.tcp.read(&mut output)
//!                 .map_err(|err| Error::Failure(err.to_string()))?;
//!
//!         Ok(output.to_vec())
//!     }
//! }
//!
//! // Oracle encrypts data.
//! impl CipherOracle for AesServiceOracle {}
//! ```
use crate::Result;

mod cipher;
mod hash;

pub use cipher::CipherOracle;
pub use cipher::DecipherOracle;
pub use hash::HashInterface;

/// Base `Oracle` trait containing the generic `process` method.
///
/// Library functions actually take more specific traits
/// (depending on this super trait),
/// like [`CipherOracle`] or [`DecipherOracle`].
///
pub trait Oracle {
    /// Takes the `input` data, and returns processed data.
    fn process(&mut self, input: &[u8]) -> Result<Vec<u8>>;
}

