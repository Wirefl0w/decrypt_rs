// Copyright (C) See full copyright notice in the COPYRIGHT file at
// the root of the crate.
//
// This file is part of decrypt_rs.
//
//   decrypt_rs is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   decrypt_rs is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with decrypt_rs.  If not, see <https://www.gnu.org/licenses/>
//

//! Error handling and error type definition.

use std::result::Result as StdResult;
use std::fmt::{Display, Formatter};

/// Type alias for a result returnnig a [`Error`].
pub type Result<T> = StdResult<T, Error>;

/// The global Error type for the whole `decrypt_rs` library.
#[derive(Debug, PartialEq)]
pub enum Error {
    /// Invalid Padding Error
    InvalidPadding,
    /// Generic failure Error.
    Failure(String),
    /// Incorrect mode of operation
    IncorrectMode(&'static str),
    /// Incorrect argument given.
    ///
    /// First `&str` is the argument name, second is the reason.
    InvalidArgument(&'static str, &'static str),
    /// Incorrect attribute setting.
    ///
    /// First `&str` is the attribute name, second is the reason.
    InvalidAttribute(&'static str, &'static str),
}

impl std::error::Error for Error {}

impl Error {
    /// Helper function to quickly build an `InvalidAttribute` for
    /// an unspecified attribute.
    pub fn unknown_attribute(attr: &'static str) -> Self {
        Error::InvalidAttribute(attr, "Attribute is unspecified or invalid.")
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        let msg = match self {
            Error::InvalidPadding                 => String::from("Invalid Padding"),
            Error::Failure(err)                   => format!("Failure: {}", err),
            Error::IncorrectMode(err)             => format!("Incorrect Mode: {}", err),
            Error::InvalidArgument(arg, reason)   => format!("Invalid Argument '{}': {}", arg, reason),
            Error::InvalidAttribute(attr, reason) => format!("Invalid Attribute: '{}': {}", attr, reason),
        };

        write!(f, "{}", msg)
    }
}

