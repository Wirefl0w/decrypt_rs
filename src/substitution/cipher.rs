// Copyright (C) See full copyright notice in the COPYRIGHT file at
// the root of the crate.
//
// This file is part of decrypt_rs.
//
//   decrypt_rs is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   decrypt_rs is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with decrypt_rs.  If not, see <https://www.gnu.org/licenses/>
//

/// A structure to perform substitution ciphers.
pub struct Cipher {
    /// The substitution function, taking an input byte and a key byte, and returning
    /// the output byte, encoded by substitution.
    ///
    /// _Example for a `xor` substitution function: `subs_fn = |b, k| c ^ k;`_
    pub subs_fn: fn(input_byte: u8, key_byte: u8) -> u8,
    /// If `true`, the non letter characters are ignored.
    pub ignore_non_letters: bool,
    /// The key used for substitution.
    pub key: Vec<u8>,
}

impl Cipher {
    /// Default constructor: creates an empty cipher.
    ///
    /// The substitution function returns the input byte as is
    /// and the key is just 1 null byte.
    pub fn new() -> Self {
        Cipher {
            subs_fn: |b, _| b,
            ignore_non_letters: false,
            key: vec![0u8],
        }
    }

    /// Change the substitution function to `xor`.
    pub fn xor(mut self) -> Self {
        self.subs_fn = |b, k| b ^ k;

        self
    }

    /// Change the substitution function to a character shift.
    ///
    /// Using a `char shift` cipher, ignore non letters by default.
    ///
    /// Ex: `subs_fn(b'A', 2) -> b'C'`
    pub fn char_shift(mut self) -> Self {
        self.subs_fn = |b, k| {
            let k = k % 26;
            if b.is_ascii_lowercase() {
                ((b - b'a' + k) % 26) + b'a'
            } else if b.is_ascii_uppercase() {
                ((b - b'A' + k) % 26) + b'A'
            } else {
                b
            }
        };

        self.ignore_non_letters = true;

        self
    }

    /// Builder methods that sets
    /// [`Cipher.ignore_non_letters`](#structfield.ignore_non_letters)
    /// to `true`.
    pub fn ignore_non_letters(mut self) -> Self {
        self.ignore_non_letters = true;

        self
    }

    /// Encode the given `input` data with the cipher `key` using
    /// the cipher substitution function.
    pub fn encode<T: AsRef<[u8]>>(&self, input: T) -> Vec<u8> {
        self.encode_with_key(input, &self.key)
    }

    /// Encode the given `input` data with the given `key` using the cipher
    /// substitution function.
    ///
    /// If the `key` is shorter than the `input`, the `key`
    /// is repeated to match the `input` length.
    ///
    /// This function ignore the cipher `key`.
    pub fn encode_with_key(&self, input: impl AsRef<[u8]>, key: impl AsRef<[u8]>) -> Vec<u8> {
        let input: &[u8] = input.as_ref();
        let key: &[u8] = key.as_ref();

        let subs_fn = self.subs_fn;

        if self.ignore_non_letters {
            let mut key_idx = 0;
            input.iter()
                 .map(|x| {
                    if x.is_ascii_alphabetic() {
                        let y = subs_fn(*x, key[key_idx]);
                        key_idx = (key_idx + 1) % key.len();
                        y
                    } else {
                        *x
                    }
                 })
                 .collect()
        } else {
            input.iter()
                 .enumerate()
                 .map(|(i, x)| subs_fn(*x, key[i % key.len()]))
                 .collect()
        }
    }

}

impl Default for Cipher {
    fn default() -> Self {
        Self::new()
    }
}
