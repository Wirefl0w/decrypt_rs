// Copyright (C) See full copyright notice in the COPYRIGHT file at
// the root of the crate.
//
// This file is part of decrypt_rs.
//
//   decrypt_rs is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   decrypt_rs is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with decrypt_rs.  If not, see <https://www.gnu.org/licenses/>
//

//! Containing pre-made character frequency tables.
//!
//! # Alphabets
//!
//! This modules defines some arrays to be used as frequency character table for
//! the [`substitution::Decoder`](super::Decoder).
//!
//! These arrays are not full-featured frequency tables, but just a list of
//! characters ordered by decreasing frequency (the first char is the most frequent).

/// An enumeration of available alphabets.
pub enum Alphabet {
    English,
    French,
}

impl Alphabet {
    /// Returns the predefined frequency table corrsponding to enum variant
    /// with the given case sensitivity.
    ///
    /// Some alphabets with given case sensitivity might be missing, and
    /// the function will panic if such missing alphabet is requested.
    pub fn get(&self, case_sensitive: bool) -> &[char] {
        match self {
            Alphabet::English => {
                if case_sensitive {
                    &FREQ_CHAR_ENGLISH_CASE
                } else {
                    panic!("No alphabets exists for NON case sensitive English");
                }
            },
            Alphabet::French => {
                if case_sensitive {
                    panic!("No alphabets exists for case sensitive French");
                } else {
                    &FREQ_CHAR_FRENCH
                }
            },
        }
    }
}

// TODO: Improve the system:
//   - Add more frequency table.
//   - Add frequency table for group of letters

/// The english case sensitive character frequency table
// " etoahnsrildumy,.wfcgIbpAETSvO'kRNLCH;WMDBUPFG?Y!-KxVjqJ:Qz91XZ"
// The space might have been arbitrarilly placed first...
pub static FREQ_CHAR_ENGLISH_CASE: [char; 61] = [' ', 'e', 't', 'o', 'a', 'h', 'n',
                                                 's', 'r', 'i', 'l', 'd', 'u', 'm',
                                                 'y', ',', '.', 'w', 'f', 'c', 'g',
                                                 'I', 'b', 'p', 'A', 'E', 'T', 'S',
                                                 'v', 'O', '\'', 'k', 'R', 'N', 'L',
                                                 'C', 'H', ';', 'W', 'M', 'D', 'B',
                                                 'U', 'P', 'F', 'G', '?', 'Y', '!',
                                                 '-', 'K', 'x', 'V', 'j', 'q', 'J',
                                                 ':', 'Q', 'z', 'X', 'Z'];


/// The French character frequency table.
// "eaisnrtoludcmpé gbvhf,.'q-yxj:èàk?wzê"ç;ôâîûùï!áüëöí"
pub static FREQ_CHAR_FRENCH: [char; 52] = ['e', 'a', 'i', 's', 'n', 'r', 't', 'o',
                                           'l', 'u', 'd', 'c', 'm', 'p', 'é', ' ',
                                           'g', 'b', 'v', 'h', 'f', ',', '.', '\'',
                                           'q', '-', 'y', 'x', 'j', ':', 'è', 'à',
                                           'k', '?', 'w', 'z', 'ê', '"', 'ç', ';',
                                           'ô', 'â', 'î', 'û', 'ù', 'ï', '!', 'á',
                                           'ü', 'ë', 'ö', 'í'];

