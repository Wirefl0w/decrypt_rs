// Copyright (C) See full copyright notice in the COPYRIGHT file at
// the root of the crate.
//
// This file is part of decrypt_rs.
//
//   decrypt_rs is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   decrypt_rs is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with decrypt_rs.  If not, see <https://www.gnu.org/licenses/>
//

use super::alphabets::Alphabet;
use super::Cipher as SubsCipher;
use crate::utils::hamming_distance;
use super::{KeyCandidates, KeyByteCandidate};

/// This structure provides a way to analyze and decode texts that are
/// encoded with substitution ciphers.
///
/// The struct is constructed using a builder pattern.\
///  - It needs a table representing all the characters expected to be present in the
/// output in a decreasing frequency order.
/// (e.g. in english: `vec![' ', 'e', 't', 'a', ...];`).
///  - The decoder can break either MonoAlphabetic ciphers (1 byte key), or
/// PolyAlphabetic ciphers.
///  - Finaly, the decoder needs a substitution function to know how to substitute
///  input bytes. This substitution function is given in the form of a
///  [`substitution::Cipher`](super::Cipher).
///
/// By default, the decoder is built for mono alphabetic cipher using a `xor`
/// substitution function.
///
/// The default constructor allows specifying a predefined alphabet using
/// the [`Alphabet`] type.
/// These constructors take a `bool` as a parameter which sets the
/// decoder as **case sensitive** if `true` (the corresponding alphabet might
/// not exists in the library resulting in a `panic!` case).
///
/// ## Example
///
/// ```no_run
/// use decrypt_rs::substitution;
/// use decrypt_rs::substitution::alphabets::Alphabet;
///
/// // Building a decoder for polyalphabetic cipher using `xor` substitution
/// // for english encrypted text
/// let mut decoder = substitution::Decoder::new(Alphabet::English, true)
///                                     .poly()
///                                     .xor();
/// //    (`.xor()` is optional as it is the default)
///
/// let input = b"...";
///
/// // Magic decoding
/// let output = decoder.decode(&input);
///
/// // The above line is equivalent to the following code:
///
/// // Default value
/// decoder.config_set_max_nb_blocks(7);
///
/// // Get the keysize
/// let keysize = decoder.find_keysize(&input, 5, 40);
/// // Break the key
/// let key = decoder.break_key_with_keysize(input, keysize)
///                     .best_guess(); // Get most probable one.
///
/// // Affect cipher key
/// decoder.cipher.key = key;
///
/// // Decode input
/// let output = decoder.cipher.encode(&input);
/// ```
pub struct Decoder {
    freq_char:      Vec<char>,

    mono_cipher:    bool,
    case_sensitive: bool,

    max_nb_blocks:  usize,
    min_keysize:    usize,
    max_keysize:    usize,

    /// The substitution cipher containing the appropriate
    /// substitution function.
    pub cipher: SubsCipher,
}

impl Decoder {
    //  ===  Builder methods  ===

    /// Structure constructor using predifined character frequency table.
    pub fn new(alphabet: Alphabet, case_sensitive: bool) -> Self {
        Self::new_with_alpha(alphabet.get(case_sensitive).to_vec())
    }

    /// Structure constructor allowing to give a custom character frequency table.
    pub fn new_with_alpha(freq_char: Vec<char>) -> Self {
        Decoder {
            freq_char,
            mono_cipher: true,
            case_sensitive: false,
            max_nb_blocks:  7,
            min_keysize:    5,
            max_keysize:   40,
            cipher: SubsCipher::new().xor(),
        }
    }


    /// Use a `xor` substitution cipher. _(default)_
    pub fn xor(mut self) -> Self {
        self.cipher = self.cipher.xor();

        self
    }

    /// Use a character shift substitution cipher.
    pub fn char_shift(mut self) -> Self {
        self.cipher = self.cipher.char_shift();

        self
    }

    /// Build a decoder for mono alphabetic substitution. _(default)_
    ///
    /// The key is `1` byte long and each byte of the text is encoded
    /// with the single key byte.
    pub fn mono(mut self) -> Self {
        self.mono_cipher = true;

        self
    }

    /// Build a decoder for poly alphabetic substitution.
    ///
    /// The key is more than `1` byte long. Each byte of the text is encoded with each byte of
    /// the key one after another, starting with the first one and repeating when reached the key
    /// length.
    pub fn poly(mut self) -> Self {
        self.mono_cipher = false;

        self
    }


    //  ===  Utils  ===

    /// Compute the score of a message regarding the decoder character frequency table.
    ///
    /// Score are given in `[0, 1]`.
    /// A higher score means that the `msg` is more probably valid output.
    pub fn compute_score<T: AsRef<[u8]>>(&self, msg: T) -> f64 {
        let msg = msg.as_ref();

        let mut score = 0f64;
        for b in msg {
            let c: char = if !self.case_sensitive && b.is_ascii_uppercase() {
                (*b).to_ascii_lowercase().into()
            } else {
                (*b).into()
            };

            // Find the index of the char in the frequency array, if it exists
            if let Some(index) = self.freq_char.iter().position(|x| x == &c) {
                // Get the score value for the char regarding its position in the list:
                // The further means less common.
                score += (self.freq_char.len() - index) as f64;
            }
        }

        // Normalize against the maximum reachable score.
        score / (msg.len() * self.freq_char.len()) as f64
    }


    //  === Breaking the cipher ===

    /// Attempts to break the substitution cipher.
    /// It first break the key and perform a substitution of the input with the key
    /// to retrieve the plaintext.
    pub fn decode<T: AsRef<[u8]>>(&mut self, input: T) -> Vec<u8> {
        let input: &[u8] = input.as_ref();

        self.break_key(input);

        self.cipher.encode(input)
    }

    /// Internal parameter representing the maximum number of blocks
    /// to take when evaluating a given key size.
    ///
    /// This value is used when trying to find key size.
    pub fn config_get_max_nb_blocks(&self) -> usize {
        self.max_nb_blocks
    }

    /// Sets the internal value of [`max_nb_blocks`](Decoder::config_get_max_nb_blocks).
    ///
    /// This value should be tweaked by a user aware of the internal
    /// struct code.
    ///
    /// Panics if `max_nb_blocks < 2`.
    pub fn config_set_max_nb_blocks(&mut self, max_nb_blocks: usize) {
        if max_nb_blocks < 2 {
            panic!("Invalid value {}: max_nb_blocks should be > 2", max_nb_blocks);
        }

        self.max_nb_blocks = max_nb_blocks;
    }

    /// Gets the values used in the [`break_key`](Decoder::break_key) function.
    ///
    /// Returns respectively the lower limit and the upper limit of
    /// the tested key sizes.
    pub fn config_get_key_size_limits(&self) -> (usize, usize) {
        (self.min_keysize, self.max_keysize)
    }

    /// Set the [limits of tested key sizes](Decoder::config_get_key_size_limits).
    ///
    /// Panics if `min_keysize >= max_keysize`.
    pub fn config_set_key_size_limits(&mut self, min_keysize: usize, max_keysize: usize) {
        if min_keysize >= max_keysize {
            panic!("Inavlid limits (min: {} & max: {}): min_keysize should be less than max_keysize",
                min_keysize, max_keysize);
        }

        self.min_keysize = min_keysize;
        self.max_keysize = max_keysize;
    }

    /// Find the most probable keysize for the given input.
    /// It will search the size in the range `[min_keysize, max_keysize]`.
    ///
    /// If the cipher is a mono alphabetic one, the function immediatelly returns `1`.
    pub fn find_keysize<T: AsRef<[u8]>>(&self, input: T, min_keysize: usize, max_keysize: usize) -> usize {
        if self.mono_cipher {
            return 1;
        }

        let input: &[u8] = input.as_ref();

        let mut best_score = f64::MAX; // The lower, the better
        let mut best_keysize = 0;

        // For each candidate keysize
        for keysize in min_keysize ..= max_keysize {
            let mut score = 0;

            // Do not take more than the input length
            let nb_blocks = input.len() / keysize;
            let nb_blocks = if nb_blocks < self.max_nb_blocks {
                self.max_nb_blocks - 1
            } else {
                nb_blocks - 1
            };

            if nb_blocks >= 2 {
                // Break the input into 'keysize' blocks
                let mut block_iter = input.chunks(keysize);

                // Initialize `block1` before the loop to reuse it
                // (giving it the next block at each iteration).
                let mut block1 = block_iter.next().unwrap();

                for _ in 0..(nb_blocks-1) {
                    // Compute the hamming distance with the next block
                    let block2: &[u8] = block_iter.next().unwrap();

                    // Normalize towards keysize
                    score += hamming_distance(block1, block2);

                    // Affect current `block2` into next `block1`
                    block1 = block2;
                }

                // Normalize agains keysize
                let mut score: f64 = score as f64 / keysize as f64;
                // Take the mean of all the values
                score /= nb_blocks as f64;

                // Key the best candidate
                if score < best_score {
                    best_keysize = keysize;
                    best_score = score;
                    log::trace!("key_size {} has score {}", keysize, score);
                }
            }
        }

        log::debug!("Found key size of {} with score {}", best_keysize, best_score);

        best_keysize
    }

    /// Attempts to break the key of the substitution cipher and return the most
    /// probable key.
    ///
    /// Once broken, the key is affected as the inner cipher key.
    ///
    /// _The key sizes limits used to find the key can be updated
    /// using the [`Decoder::config_set_key_size_limits`] method._
    pub fn break_key<T: AsRef<[u8]>>(&mut self, input: T) -> Vec<u8> {
        let input: &[u8] = input.as_ref();

        let key: KeyCandidates;

        if self.mono_cipher {
            key = self.break_key_with_keysize(input, 1);
        } else {
            // Find the keysize
            let keysize = self.find_keysize(input, self.min_keysize, self.max_keysize);

            log::debug!("[ ] Breaking multi-xor key with key size {}:", keysize);
            key = self.break_key_with_keysize(input, keysize);
            log::debug!("[+] Done");
        }

        let key = key.best_guess();

        self.cipher.key = key.clone();

        key
    }

    /// Attempts to break the key of the substitution cipher knowing its
    /// keysize, ignoring the 'mono/poly' attribute of the decoder.
    ///
    /// This function is used by [`Decoder::break_key`] to retrieve the most probable key.
    ///
    /// _This function do not update the inner cipher object._
    pub fn break_key_with_keysize(&mut self, input: &[u8], key_size: usize) -> KeyCandidates {
        // Break the input into `keysize` blocks
        // and make groups with each n-th byte of each blocks
        let mut blocks: Vec<Vec<u8>> = Vec::new();
        log::trace!("[ ] Breaking key of size {}:", key_size);
        for b in 0..key_size {
            let block: Vec<u8> = input.iter().enumerate()
                                              .filter(|(i, _)| (i % key_size) == b)
                                              .map(|(_, x)| *x)
                                              .collect();
            blocks.push(block);
        }

        log::trace!("Breaking {} blocks", blocks.len());

        // Solve each resulting block as a mono alphabetic substitution
        let mut key = KeyCandidates::new(key_size);
        for (idx, block) in blocks.iter().enumerate() {
            log::trace!("[ ] Breaking block {}", idx+1);
            let key_c = self.break_key_mono(block.as_ref());
            key.add_candidates(idx, key_c);
        }

        log::trace!("[+] Done");

        key
    }


    /// Try to find the best key for a mono alphabetic cipher
    ///
    /// Try to decode with all possible bytes and keep the output
    /// with the best score.
    fn break_key_mono(&self, input: &[u8]) -> KeyCandidates {
        let mut key_c = KeyCandidates::new(1);

        for key in 1u8 ..= 255 {
            let try_dec = self.cipher.encode_with_key(input, [key]);

            let score = self.compute_score(&try_dec);

            if score > 0f64 {
                let keybyte = KeyByteCandidate {
                    byte: key,
                    score
                };

                key_c.add_candidate(0, keybyte);
            }
        }

        key_c
    }
}
