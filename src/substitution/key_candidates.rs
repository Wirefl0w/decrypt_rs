// Copyright (C) See full copyright notice in the COPYRIGHT file at
// the root of the crate.
//
// This file is part of decrypt_rs.
//
//   decrypt_rs is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   decrypt_rs is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with decrypt_rs.  If not, see <https://www.gnu.org/licenses/>
//

use std::cmp::{Ordering, Reverse};

/// Represents a possible key byte.
/// It holds both the byte and its computed score.
#[derive(Clone, Copy, Debug)]
pub struct KeyByteCandidate {
    /// The key byte.
    pub byte: u8,
    /// The associated score.
    /// Scores are in `[0, 1]`, the higher, the better.
    pub score: f64,
}

impl PartialEq for KeyByteCandidate {
    fn eq(&self, other: &Self) -> bool {
        self.score.eq(&other.score)
    }
}

impl PartialOrd for KeyByteCandidate {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.score.partial_cmp(&other.score)
    }
}

/// Represents different key possibilities.
///
/// It contains all the [`KeyByteCandidate`]s for the key.
#[derive(Clone)]
pub struct KeyCandidates {
    bytes: Vec<Vec<KeyByteCandidate>>,
}

impl KeyCandidates {
    /// Construct empty KeyCandidates of given size.
    pub fn new(key_size: usize) -> Self {
        KeyCandidates {
            bytes: vec![Vec::new(); key_size]
        }
    }

    /// Add the given byte candidate at the given index of the key.
    ///
    /// Panics if `idx >= key_size`.
    pub fn add_candidate(&mut self, idx: usize, candidate: KeyByteCandidate) {
        if idx >= self.key_size() {
            panic!("Given idx is higher than key size");
        }

        self.bytes[idx].push(candidate);
        self.sort_row(idx);
    }

    /// Add multiple candidates for a given key byte.
    ///
    /// Panics if `candidates.key_size != 1` and if `idx >= key_size`.
    pub fn add_candidates(&mut self, idx: usize, candidates: KeyCandidates) {
        if candidates.key_size() != 1 {
            panic!("Candidates key_size can only be 1");
        }
        if idx >= self.key_size() {
            panic!("Given idx is higher than key size");
        }

        self.bytes[idx].append(&mut candidates.bytes[0].clone());
        self.sort_row(idx);
    }

    /// Returns the size of the key.
    pub fn key_size(&self) -> usize {
        self.bytes.len()
    }

    /// Returns the key composed by the most probable
    /// key bytes.
    pub fn best_guess(&self) -> Vec<u8> {
        self.bytes.iter().map(|x| {
            x.first().expect("KeyCandidates contains empty KeyByte candidate")
                .byte
            }).collect()
    }

    /// Returns a new [`KeyCandidates`] by filtering all [`KeyByteCandidate`]
    /// which have a score lower than te given minimum.
    pub fn filter(&self, minimum_score: f64) -> Self {
        let bytes = self.bytes.iter()
                              .map(|v| {
                                    v.iter()
                                    .filter( |k| k.score >= minimum_score )
                                    .copied()
                                    .collect()
                                }).collect();
        KeyCandidates { bytes }
    }

    /// Creates a new [`KeyCandidates`] that only contains the
    /// best `max_candidates` elements.
    pub fn limit(&self, max_candidates: usize) -> Self {
        let bytes = self.bytes.iter()
                              .map(|v| {
                                  v.iter()
                                   .take(max_candidates)
                                   .copied()
                                   .collect()
                                }).collect();
        KeyCandidates { bytes }
    }

    /// Order all candidates at a given index by score.
    /// The best elements (higher score) first.
    fn sort_row(&mut self, idx: usize) {
        self.bytes[idx].sort_by(
            |a, b| Reverse(a).partial_cmp(&Reverse(b)).unwrap());
    }
}

