//! Example of the implementation of the trait [`HashInterface`] for
//! a Sha1 Hash function.

use std::env::args;

use decrypt_rs::{Result, Error};
use decrypt_rs::oracle::HashInterface;
use decrypt_rs::utils::hex;

fn main() {
    let input = args().skip(1).next();

    if let Some(input) = input {
        println!("{}", hex::encode(Sha1::hash(input)));
    } else {
        println!("Usage: {} <input>", args().next().unwrap());
    }
}


/// SHA1 Hash function.
///
/// The following implementation does not provide an `update`
/// mechanism with input buffering.
pub struct Sha1 {
    h0: u32,
    h1: u32,
    h2: u32,
    h3: u32,
    h4: u32,
}

impl Sha1 {
    const DIGEST_SIZE: usize = 20;
    const BLOCK_SIZE: usize  = 64;

    /// Initialize `Sha1` hashing function.
    pub fn new() -> Self {
        let h0 = 0x67452301;
        let h1 = 0xEFCDAB89;
        let h2 = 0x98BADCFE;
        let h3 = 0x10325476;
        let h4 = 0xC3D2E1F0;

        Sha1 {
            h0,
            h1,
            h2,
            h3,
            h4,
        }
    }

    /// Reset hash state.
    pub fn reset(&mut self) {
        *self = Self::new();
    }

    /// Hash the whole input at once.
    pub fn hash(input: impl AsRef<[u8]>) -> Vec<u8> {
        let mut sha1 = Self::new();
        sha1.digest(input)
    }

    /// Generate the padding for the given input size.
    fn padding(input_len: usize) -> Vec<u8> {
        let len_mod = (input_len + 1) % 64;

        let zeroes_len = if len_mod < Self::BLOCK_SIZE - 8 {
            Self::BLOCK_SIZE - 8 - len_mod
        } else {
            Self::BLOCK_SIZE*2 - 8 - len_mod
        };

        let mut pad = Vec::with_capacity(1 + zeroes_len + 8);

        pad.push(0x80);
        pad.append(&mut vec![0u8; zeroes_len]);

        let n_bits: u64 = input_len as u64 * 8;
        pad.append(&mut n_bits.to_be_bytes().to_vec());

        pad
    }

    /// Hash the given data as a final input from the state of the
    /// current instance.
    ///
    /// (Add padding and process blocks.)
    pub fn digest(&mut self, input: impl AsRef<[u8]>) -> Vec<u8> {
        let input = input.as_ref();

        let mut padding = Self::padding(input.len());

        let full_len = input.len() + padding.len();
        assert_eq!(full_len % Self::BLOCK_SIZE, 0);

        let mut padded_input = Vec::with_capacity(full_len);

        padded_input.append(&mut input.to_vec());
        padded_input.append(&mut padding);

        let output = self.process(padded_input);
        self.reset();

        output
    }

    /// Process given blocks of input
    /// (input should be an exact number of block).
    fn process(&mut self, input: impl AsRef<[u8]>) -> Vec<u8> {
        let input = input.as_ref();
        assert!(input.len() % Self::BLOCK_SIZE == 0);

        let mut w = [0u32; 80];

        for chunk in input.chunks_exact(Self::BLOCK_SIZE) {
            for (w_i, word) in w.iter_mut().zip(chunk.chunks_exact(4)) {
                *w_i = u32::from_be_bytes(word.try_into().unwrap());
            }

            for i in 16..80 {
                w[i] = (w[i-3] ^ w[i-8] ^ w[i-14] ^ w[i-16]).rotate_left(1);
            }


            let mut a = self.h0;
            let mut b = self.h1;
            let mut c = self.h2;
            let mut d = self.h3;
            let mut e = self.h4;

            for (i, w_i) in w.iter().enumerate() {
                let f: u32;
                let k: u32;

                if i < 20 {
                    f = (b & c) ^ ((!b) & d);
                    k = 0x5A827999;
                } else if i < 40 {
                    f = b ^ c ^ d;
                    k = 0x6ED9EBA1;
                } else if i < 60 {
                    f = (b & c) ^ (b & d) ^ (c & d);
                    k = 0x8F1BBCDC;
                } else {
                    f = b ^ c ^ d;
                    k = 0xCA62C1D6;
                }

                let tmp = a.rotate_left(5).wrapping_add(f)
                                   .wrapping_add(e)
                                   .wrapping_add(k)
                                   .wrapping_add(*w_i);

                e = d;
                d = c;
                c = b.rotate_left(30);
                b = a;
                a = tmp;
            }

            self.h0 = self.h0.wrapping_add(a);
            self.h1 = self.h1.wrapping_add(b);
            self.h2 = self.h2.wrapping_add(c);
            self.h3 = self.h3.wrapping_add(d);
            self.h4 = self.h4.wrapping_add(e);
        }

        let mut digest = Vec::with_capacity(Self::DIGEST_SIZE);
        digest.append(&mut self.h0.to_be_bytes().to_vec());
        digest.append(&mut self.h1.to_be_bytes().to_vec());
        digest.append(&mut self.h2.to_be_bytes().to_vec());
        digest.append(&mut self.h3.to_be_bytes().to_vec());
        digest.append(&mut self.h4.to_be_bytes().to_vec());

        digest
    }
}


impl HashInterface for Sha1 {
    fn digest_size(&self) -> usize {
        Sha1::DIGEST_SIZE
    }

    fn block_size(&self) -> usize {
        Sha1::BLOCK_SIZE
    }

    fn padding(&self, input_size: usize) -> Result<Vec<u8>> {
        Ok(Sha1::padding(input_size))
    }

    fn set_state(&mut self, digest: &[u8]) -> Result<()> {
        if digest.len() != Self::DIGEST_SIZE {
            return Err(Error::InvalidArgument("state", "Sha1 state is 5 x 32 bits (20 bytes)"));
        }

        let mut state = digest.chunks_exact(4)
                            .map(|x| u32::from_be_bytes(x.try_into().unwrap()));
            

        self.h0 = state.next().unwrap();
        self.h1 = state.next().unwrap();
        self.h2 = state.next().unwrap();
        self.h3 = state.next().unwrap();
        self.h4 = state.next().unwrap();

        Ok(())
    }

    fn process_blocks(&mut self, input: &[u8]) -> Result<Vec<u8>> {
        if input.len() % Self::BLOCK_SIZE != 0 {
            Err(Error::InvalidArgument("input", "Input should be aligned with Sha1 block size."))
        } else {
            Ok(self.process(input))
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_padding() {
        assert_eq!(Sha1::padding(0), b"\x80\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00");
        assert_eq!(Sha1::padding(137), b"\x80\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x04\x48");
        assert_eq!(Sha1::padding(420), b"\x80\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0d\x20");
    }


    #[test]
    fn test_hash() {
        // Test vectors taken from RFC 3174

        let inputs = [
            b"".as_ref(),
            b"abc".as_ref(),
            &b"a".repeat(1_000_000),
            b"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq".as_ref(),
            &b"01234567".repeat(80),
        ];

        let digests = [
            b"\xda\x39\xa3\xee\x5e\x6b\x4b\x0d\x32\x55\xbf\xef\x95\x60\x18\x90\xaf\xd8\x07\x09",
            b"\xa9\x99\x3e\x36\x47\x06\x81\x6a\xba\x3e\x25\x71\x78\x50\xc2\x6c\x9c\xd0\xd8\x9d",
            b"\x34\xaa\x97\x3c\xd4\xc4\xda\xa4\xf6\x1e\xeb\x2b\xdb\xad\x27\x31\x65\x34\x01\x6f",
            b"\x84\x98\x3e\x44\x1c\x3b\xd2\x6e\xba\xae\x4a\xa1\xf9\x51\x29\xe5\xe5\x46\x70\xf1",
            b"\xde\xa3\x56\xa2\xcd\xdd\x90\xc7\xa7\xec\xed\xc5\xeb\xb5\x63\x93\x4f\x46\x04\x52",
        ];

        let mut i = 0;
        for (input, digest) in inputs.iter().zip(digests) {
            println!("input {}", i);
            i+=1;
            assert_eq!(
                &Sha1::hash(input),
                digest
            );
        }
    }
}
