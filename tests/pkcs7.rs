use decrypt_rs::Error;
use decrypt_rs::utils::pkcs7;

#[test]
fn pcks7_padding_unpadding() {
    let input = b"YELLOW SUBMARINE";
    let pad_size = 20;
    let expected = b"YELLOW SUBMARINE\x04\x04\x04\x04";

    let padded = pkcs7::pad(input, pad_size);

    assert_eq!(padded, expected);
    assert_eq!(pkcs7::unpad(padded), Ok(input.to_vec()));
}

#[test]
fn incorrect_padding() {
    let valid_input = b"ICE ICE BABY\x04\x04\x04\x04";
    let valid_unpad = b"ICE ICE BABY".to_vec();

    let invalid_inputs = [
        b"ICE ICE BABY\x05\x05\x05\x05".to_vec(),
        b"ICE ICE BABY\x01\x02\x03\x04".to_vec(),
        b"ICE ICE BABY\x10".to_vec(),
        b"ICE ICE BABY\x00".to_vec(),
    ];

    assert_eq!(pkcs7::unpad(valid_input), Ok(valid_unpad));
    assert!(matches!(pkcs7::unpad(b""), Err(Error::InvalidArgument(_, _))));

    for invalid in invalid_inputs {
        assert_eq!(pkcs7::unpad(invalid), Err(Error::InvalidPadding));
    }
}
    
