use decrypt_rs::substitution;
use decrypt_rs::substitution::alphabets::Alphabet;


#[test]
fn single_xor_decode() {
    let input = b"\x1b\x37\x37\x33\x31\x36\x3f\x78\x15\x1b\x7f\x2b\x78\x34\x31\x33\x3d\x78\x39\x78\x28\x37\x2d\x36\x3c\x78\x37\x3e\x78\x3a\x39\x3b\x37\x36";
    let expected = "Cooking MC's like a pound of bacon";

    let mut decoder = substitution::Decoder::new(Alphabet::English, true)
                                      .xor()
                                      .mono();

    let output = decoder.decode(&input);

    let msg = String::from_utf8(output).expect("Decoded message isn't valid");

    assert_eq!(msg, expected)
}

#[test]
#[ignore]
fn multi_xor_decode() {
    let expected_key_str = "Terminator X: Bring the noise";
    let expected = include_str!("substitution/multi_xor_decode_dec.txt");

    let ciphertext = hex::decode(
        &include_str!("substitution/multi_xor_decode.txt").replace("\n", "")
        ).unwrap();

    let mut decoder = substitution::Decoder::new(Alphabet::English, true)
                                      .xor()
                                      .poly();

    let key = decoder.break_key(&ciphertext);
    let key_str = String::from_utf8(key.clone()).unwrap();

    println!("[+] Found xor key: '{}'", key_str);
    assert_eq!(&key_str, expected_key_str);

    let computed = decoder.cipher.encode(ciphertext);

    let msg = String::from_utf8(computed).unwrap();
    assert_eq!(msg, expected);
}

mod hex {
    use std::num::ParseIntError;

    pub fn decode(s: &str) -> Result<Vec<u8>, ParseIntError> {
        (0..s.len())
            .step_by(2)
            .map(|i| u8::from_str_radix(&s[i..i + 2], 16))
            .collect()
    }
}

